# ATMPT
A SPWN testing framework.

# Usage
```rs
let expect = import atmpt;
@test::new("nineteen", () {
	expect(9 + 10).toBe(19);
});
```
Output:
```
✅ Test nineteen succeeded.
```
# Download
Download the ATMPT .zip file from [Releases](https://gitlab.com/Unzor/atmpt/-/releases), unzip it, and rename the folder to "libraries".
